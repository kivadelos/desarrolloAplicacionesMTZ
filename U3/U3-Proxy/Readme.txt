-Patrones de diseño-

PROXY

El patrón proxy trata de proporcionar un objeto intermediario 
que represente o sustituya al objeto original con motivo de 
controlar el acceso y otras características del mismo.

Se aplica cuando:

El patrón Proxy se usa cuando se necesita una referencia a un objeto más flexible o sofisticada que un puntero. 

Según la funcionalidad requerida podemos encontrar varias aplicaciones:
•	Proxy remoto: representa un objeto en otro espacio de direcciones.
•	Proxy virtual: retrasa la creación de objetos costosos.
•	Proxy de protección: controla el acceso a un objeto.
•	Referencia inteligente: tiene la misma finalidad que un puntero 
        pero además ejecuta acciones adicionales sobre el objeto, 
        como por ejemplo el control de concurrencia.


Además, su uso también permite realizar una optimización COW (copy-on-write) , 
puesto que copiar un objeto grande puede ser costoso, y si la copia no se modifica, 
no es necesario incurrir en dicho gasto. Además el sujeto mantiene un número de referencias,
y sólo cuando se realiza una operación que modifica el objeto, éste se copia. 
Es útil por tanto para retrasar la replicación de un objeto hasta que cambia. 