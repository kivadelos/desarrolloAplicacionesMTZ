-Patrones de dise�o-

FACTORY
Uno de los patrones de dise�o m�s utilizados en Java 
es el patron Factory que es un patr�n de dise�o creacional 
y que sirve para construir una jerarqu�a de clases. 
Sin embargo a veces a la gente le cuesta ver como usar este patr�n en su c�digo. 


En dise�o de software, el patr�n de dise�o Factory Method 
consiste en utilizar una clase constructora (al estilo del Abstract Factory) abstracta 
con unos cuantos m�todos definidos y otro(s) abstracto(s): 
el dedicado a la construcci�n de objetos de un subtipo de un tipo determinado. 
Es una simplificaci�n del Abstract Factory, en la que la clase abstracta tiene 
m�todos concretos que usan algunos de los abstractos; 
seg�n usemos una u otra hija de esta clase abstracta, 
tendremos uno u otro comportamiento.

El patr�n de dise�o �Factory� se enfoca en la creaci�n de una interface
que facilite la creaci�n de objetos que se organizan por diferentes subclases. 
Esto ocurre con frecuencia cuando se hace uso de la herencia.
Una clase abstracta se genera conteniendo los atributos generales, 
y despu�s se crean clases para objetos espec�ficos. 
Para evitar llamar constructores espec�ficos se deben crear interfaces que nos ayuden en estas tareas.

En este patr�n de dise�o hay dos formas de generar las interfaces para crear objetos:
 El m�todo Factory (Factory method) y el �Abstract Factory�.