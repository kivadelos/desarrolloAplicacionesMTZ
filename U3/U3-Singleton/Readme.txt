-Patrones de dise�o-

SINGLETON

En ingenier�a de software, singleton o instancia �nica es un patr�n de dise�o que permite restringir la creaci�n de objetos pertenecientes a una clase o el valor de un tipo a un �nico objeto.

Su intenci�n consiste en garantizar que una clase solo tenga una instancia y proporcionar un punto de acceso global a ella.

El patr�n singleton se implementa creando en nuestra clase un m�todo que crea una instancia del objeto solo si todav�a no existe alguna. Para asegurar que la clase no puede ser instanciada nuevamente se regula el alcance del constructor (con modificadores de acceso como protegido o privado).

La instrumentaci�n del patr�n puede ser delicada en programas con m�ltiples hilos de ejecuci�n. Si dos hilos de ejecuci�n intentan crear la instancia al mismo tiempo y esta no existe todav�a, solo uno de ellos debe lograr crear el objeto. La soluci�n cl�sica para este problema es utilizar exclusi�n mutua en el m�todo de creaci�n de la clase que implementa el patr�n.

Las situaciones m�s habituales de aplicaci�n de este patr�n son aquellas en las que dicha clase controla el acceso a un recurso f�sico �nico (como puede ser el rat�n o un archivo abierto en modo exclusivo) o cuando cierto tipo de datos debe estar disponible para todos los dem�s objetos de la aplicaci�n.

El patr�n singleton provee una �nica instancia global gracias a que:

La propia clase es responsable de crear la �nica instancia.
Permite el acceso global a dicha instancia mediante un m�todo de clase.
Declara el constructor de clase como privado para que no sea instanciable directamente.
Al estar internamente autoreferenciada, en lenguajes como Java, el recolector de basura no act�a.

La intenci�n de este patr�n es garantizar 
que solamente pueda existir una �nica instancia 
de una determinada clase y que exista 
una referencia global en toda la aplicaci�n.