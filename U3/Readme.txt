-Pruebas unitarias-

En programaci�n, una prueba unitaria es una forma de comprobar el correcto funcionamiento de una unidad de c�digo. 
Por ejemplo en dise�o estructurado o en dise�o funcional una funci�n o un procedimiento, 
en dise�o orientado a objetos una clase. Esto sirve para asegurar que cada unidad 
funcione correctamente y eficientemente por separado. 
Adem�s de verificar que el c�digo hace lo que tiene que hacer, 
verificamos que sea correcto el nombre, los nombres y tipos de los par�metros, 
el tipo de lo que se devuelve, que si el estado inicial es v�lido entonces el estado final es v�lido.

La idea es escribir casos de prueba para cada funci�n no trivial o m�todo en el m�dulo, 
de forma que cada caso sea independiente del resto. Luego, con las Pruebas de Integraci�n, 
se podr� asegurar el correcto funcionamiento del sistema o subsistema en cuesti�n.




En cuanto a las pruebas unitarias lo ejecutaremos con unos datos de entrada (casos de prueba) 
para verificar que el funcionamiento cumple los requisitos esperados. 
Las pruebas podran ser en cada uno de los m�dulos que compondran el programa.

Esto seria a traves de la herramienta de jUnit la cual es una libreria desarrollada para probar el funcionamiento
de las clases y metodos  que componen nuestra aplicacion.