-Patrones de dise�o-

BUILDER

Como Patr�n de dise�o, el patr�n builder (Constructor) es usado
 para permitir la creaci�n de una variedad de objetos complejos 
desde un objeto fuente (Producto), el objeto fuente se compone 
de una variedad de partes que contribuyen individualmente a la 
creaci�n de cada objeto complejo a trav�s de un conjunto de 
llamadas a interfaces comunes de la clase Abstract Builder.

El patr�n builder es creacional.

A menudo, el patr�n builder construye el patr�n Composite, un patr�n estructural.

Intenci�n: Abstrae el proceso de creaci�n de un objeto complejo, 
centralizando dicho proceso en un �nico punto, 
de tal forma que el mismo proceso de construcci�n 
pueda crear representaciones diferentes.

Ventajas
�	Reduce el acoplamiento. 
�	Permite variar la representaci�n interna de estructuras complejas, respetando la interfaz com�n de la clase Builder. 
�	Se independiza el c�digo de construcci�n de la representaci�n. Las clases concretas que tratan las representaciones 
        internas no forman parte de la interfaz del Builder. 
�	Cada ConcreteBuilder tiene el c�digo espec�fico para crear y modificar una estructura interna concreta. 
�	Distintos Director con distintas utilidades (visores, parsers, etc) pueden utilizar el mismo ConcreteBuilder. 
�	Permite un mayor control en el proceso de creaci�n del objeto. 
        El Director controla la creaci�n paso a paso, solo cuando el Builder ha terminado de construir el objeto lo recupera el Director. 

Se debe utilizar este patr�n cuando sea necesario:
Independizar el algoritmo de creaci�n de un objeto complejo de las partes que constituyen el objeto y c�mo se ensamblan entre ellas.
Que el proceso de construcci�n permita distintas representaciones para el objeto construido, de manera din�mica.
Esta patr�n debe utilizarse cuando el algoritmo para crear un objeto suele ser complejo e implica la interacci�n 
de otras partes independientes y una coreograf�a entre ellas para formar el ensamblaje. 
Por ejemplo: la construcci�n de un objeto Computadora, se compondr� de otros muchos objetos, 
como puede ser un objeto PlacaDeSonido, Procesador, PlacaDeVideo, Gabinete, Monitor, etc. 
