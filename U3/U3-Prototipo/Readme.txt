-Patrones de dise�o-

PROTOTIPO

El patr�n de dise�o prototipo tiene como finalidad crear nuevos objetos 
clonando una instancia creada previamente. Este patr�n especifica la clase 
de objetos a crear mediante la clonaci�n de un prototipo que es una instancia ya creada. 
La clase de los objetos que servir�n de prototipo 
deber� incluir en su interfaz la manera de solicitar una copia, 
que ser� desarrollada luego por las clases concretas de prototipos.

Este patr�n resulta �til en escenarios donde es impreciso abstraer la l�gica que 
decide qu� tipos de objetos utilizar� una aplicaci�n, de la l�gica que luego usar�n 
esos objetos en su ejecuci�n. Los motivos de esta separaci�n pueden ser variados, 
por ejemplo, puede ser que la aplicaci�n deba basarse en alguna configuraci�n o par�metro 
en tiempo de ejecuci�n para decidir el tipo de objetos que se debe crear. 
En ese caso, la aplicaci�n necesitar� crear nuevos objetos a partir de modelos. 
Estos modelos, o prototipos, son clonados y el nuevo objeto ser� una copia exacta de los mismos, 
con el mismo estado. Esto resulta interesante para crear, en tiempo de ejecuci�n, 
copias de objetos concretos inicialmente fijados, o tambi�n cuando s�lo existe 
un n�mero peque�o de combinaciones diferentes de estado para las instancias de una clase.

Dicho de otro modo, este patr�n propone la creaci�n de distintas variantes de objetos que 
la aplicaci�n necesite, en el momento y contexto adecuado. Toda la l�gica necesaria para 
la decisi�n sobre el tipo de objetos que usar� la aplicaci�n en su ejecuci�n se hace independiente, 
de manera que el c�digo que utiliza estos objetos solicitar� una copia del objeto que necesite. 
En este contexto, una copia significa otra instancia del objeto. 
El �nico requisito que debe cumplir este objeto es suministrar la funcionalidad de clonarse.